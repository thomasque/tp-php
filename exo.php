<!DOCTYPE HTML>
<html>

<head>
<title> mysqli </title>
<meta charset="utf-8"/>
</head>

<body>
<?php
$connexion = mysqli_connect("bts.bts-malraux72.net", "t.dupont", "passe", "t.dupont", "63330");
if(mysqli_connect_errno($connexion))
{
	die('connexion impossible : ' . mysqli_connect_error());
}
else
{
	$resultat = mysqli_query($connexion, "SELECT num, nom, prenom, adrRue, adrCP, adrVille, tel, mel FROM Adherent;");
	if($resultat != false){
		$nombre_tuples = mysqli_num_rows($resultat);
		print ("La requête a retournée " . $nombre_tuples . " tuple");
		print ($nombre_tuples>1 ? 's':'');
		print ("\n");
		print ("num/nom|prenom/tel|mel/adrVille|adrCP/adrRue|");
		print ("\n");

		$numero_tuple = 1;
		while($tuple = mysqli_fetch_assoc($resultat)){
			print($numero_tuple . "/" . $tuple['prenom'] . "|" . $tuple['nom'] . "/" . $tuple['tel'] . "|" . $tuple['mel'] . "/ " . $tuple['adrVille'] . "|" . $tuple['adrCP'] . "/" . $tuple['adrRue'] . "|");
			print ("\n");
			if($numero_tuple == 5 or $numero_tuple == 10 or $numero_tuple == 15){
			print ("num/nom|prenom/tel|mel/adrVille|adrCP/adrRue|");
			print ("\n");
			}

			$numero_tuple++;
		}
		if(is_resource($resultat)){
			mysqli_free_result($resultat);
		}

	}
	mysqli_close($connexion);
}
?>
</body>
</html>

